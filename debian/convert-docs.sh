#!/bin/bash -e

MAINDIR=$(pwd)
VERBOSE=
test "$DH_VERBOSE" == "1" && VERBOSE=--verbose

cd debian/sylpheed-doc/usr/share/doc/sylpheed-doc || exit 1

for d in FAQ/de FAQ/en FAQ/es FAQ/fr FAQ/it manual/de manual/en manual/es manual/fr; do
    echo "Converting ${d}"
    test -d ${d} || exit 1
    for f in ${d}/*.html; do
        mv $VERBOSE ${f} ${f}.old
        iconv $VERBOSE -c -f ISO-8859-1 -t UTF-8 -o ${f} ${f}.old
        rm $VERBOSE -f ${f}.old
    done
done

for d in FAQ/zh_TW.Big5; do
    echo "Converting ${d}"
    test -d ${d} || exit 1
    for f in ${d}/*.html; do
        mv $VERBOSE ${f} ${f}.old
        iconv $VERBOSE -c -f BIG-5 -t UTF-8 -o ${f} ${f}.old
        rm $VERBOSE -f ${f}.old
    done
done
